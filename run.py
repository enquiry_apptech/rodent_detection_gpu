
from detect import *
import os
 
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AppTech Rodent Detection')
    parser.add_argument(
        '--video_path', type=str, default="/home/apptech/Downloads/6857b889-e74c-48ae-91d4-d8b49da28b10/sample video/video/", help='video dir')
    parser.add_argument(
        '--ann_dir', type=str, default="~/attached_storage/", help='annotation dir')
    parser.add_argument(
        '--score', type=float, default=0.98, help='bbox score threshold')
    args = parser.parse_args()

    video_list = os.listdir(args.video_path)
    for video in video_list:
        video_dir = os.path.join(args.video_path,video)
        #time = (start_time, end_time,email_sent_time)
        sever = Server(args,video_dir)
        sever.run()
        #sever.main(cam_dir="rtsp:root:pass@192.168.1.90/axis-media/media.amp?",top=50,show=True)



