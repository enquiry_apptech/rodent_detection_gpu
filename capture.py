import os
import argparse
import cv2


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AppTech Rodent Detection')
    parser.add_argument(
        '--video_path', type=str, default="/home/apptech/Downloads/6857b889-e74c-48ae-91d4-d8b49da28b10/sample video/video/", help='video dir')
  
    args = parser.parse_args()

    video_list = os.listdir(args.video_path)
    for video in video_list:
        video = args.video_path + video
        print(video)
        cap = cv2.VideoCapture(video)
        fps = cap.get(cv2.CAP_PROP_FPS)
        success,image = cap.read()
        print(image)
        #time = (start_time, end_time,email_sent_time)
 